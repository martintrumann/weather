<?php
$cacheTime = 1800;

$station = !empty($_GET["station"]) ? $_GET["station"] : "kuressaare linn";

if(file_exists(".cache/forecast.xml") && filemtime(".cache/forecast.xml") > time() - $cacheTime){
	$xml = file_get_contents(".cache/forecast.xml");
}else{
	$xml = file_get_contents("https://www.ilmateenistus.ee/ilma_andmed/xml/forecast.php");
	file_put_contents(".cache/forecast.xml", $xml);
}

if(file_exists(".cache/observetions.xml") && filemtime(".cache/observetions.xml") > time() - $cacheTime){
	$obsXml = file_get_contents(".cache/observetions.xml");
}else{
	$obsXml = file_get_contents("https://www.ilmateenistus.ee/ilma_andmed/xml/observations.php");
	file_put_contents(".cache/observetions.xml", $obsXml);
}

$data = new SimpleXMLElement($xml);
$obsData = new SimpleXMLElement($obsXml);

$obs = json_decode(json_encode($obsData))->station;

$st = array_filter($obs, function($s){global $station; return strtolower($s->name) == $station;})[0];
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>
</head>
<body style="max-width: 900px">
	<h1>Täpne</h1>
	<?php print_r($st->name) ?>
	<?php print_r($st->airtemperature) ?>

	<h1>Ilmateenistus.ee ennustus</h1>
	<?php foreach($data->forecast as $forecast): ?>
		<h2><?= $forecast["date"]?>:</h2>

		<h3>Päev:</h3>
		<div>
			<?= $forecast->day->text ?>
			<ul>
				<?php foreach($forecast->day->place as $place): ?>
					<li> <?= $place->name . " " . $place->tempmax . " &deg;C" ?> </li>
				<?php endforeach ?>
			</ul>
		</div>

		<h3>Öö:</h3>
		<div>
			<?= $forecast->night->text ?>
			<ul>
				<?php foreach($forecast->night->place as $place): ?>
					<li> <?= $place->name . " " . $place->tempmin . " &deg;C" ?> </li>
				<?php endforeach ?>
			</ul>
		</div>
	<?php endforeach ?>
</body>
</html>
